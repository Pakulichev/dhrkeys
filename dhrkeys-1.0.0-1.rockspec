package = "dhrkeys"
version = "1.0.0-1"
source = {
   url = "git+https://gitlab.com/Pakulichev/dhrkeys.git",
   tag = "v.1.0.0"
}
description = {
   summary = "dhrkeys4moonloader",
   homepage = "https://gitlab.com/Pakulichev/dhrkeys",
   license = "MIT"
}
dependencies = {
   "lua >= 5.1, < 5.4"
}
build = {
   type = "builtin",
   modules = {}
}
